package ru.pixland.potion.potion;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import ru.pixland.potion.event.PlayerDamageByPlayerEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

@Data
@Builder(builderMethodName = "newBuilder")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Potion {

    public static final Map<Player, Potion> POTION_PLAYER_MAP = new HashMap<>();

    String potionId;
    String potionName;
    PotionEffect effect;
    Consumer<PlayerDamageByPlayerEvent> onPlayerDamaged;

    /**
     * Наложить зелье на игрока
     */
    public void infect(Player player) {
        POTION_PLAYER_MAP.put(player, this);
        player.addPotionEffect(effect);
        player.sendMessage("§e§lPotions §8:: §fВы были инфицированы зельем §a" + potionName);
    }
}
