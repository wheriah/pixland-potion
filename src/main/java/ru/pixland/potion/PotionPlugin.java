package ru.pixland.potion;

import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;
import ru.pixland.potion.command.GetPotionCommand;
import ru.pixland.potion.listener.EventExecutorListener;
import ru.pixland.potion.listener.PotionListener;

public class PotionPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        // Регистрация листенера, который вызывает кастомные евенты, для упрощения
        getServer().getPluginManager().registerEvents(new EventExecutorListener(), this);
        // Регистрация листенера зелий
        getServer().getPluginManager().registerEvents(new PotionListener(), this);
        // Регистрация команды
        new GetPotionCommand().register(this);
    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
    }
}
