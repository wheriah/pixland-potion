package ru.pixland.potion.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import ru.pixland.potion.event.PlayerDamageByPlayerEvent;

public class EventExecutorListener implements Listener {
    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
        Entity damager = event.getDamager();
        Entity target = event.getEntity();

        if (damager instanceof Player && target instanceof Player) {
            PlayerDamageByPlayerEvent playerDamageByPlayerEvent = new PlayerDamageByPlayerEvent((Player) damager, (Player) target, event.getCause(), event.getDamage());
            Bukkit.getPluginManager().callEvent(playerDamageByPlayerEvent);

            event.setCancelled(playerDamageByPlayerEvent.isCancelled());
        }
    }
}
